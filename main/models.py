# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin
import os.path


class Advert(models.Model):
    TYPE_CHOICES = (
        (0, 'потерян'),
        (1, 'найден'),
    )
    advtype = models.IntegerField(max_length=1, choices = TYPE_CHOICES)
    title = models.CharField(max_length=30)
    body = models.TextField(max_length=250)
    longitude = models.FloatField()
    latitude = models.FloatField()
    city = models.CharField(max_length=80, blank=True)
    country = models.CharField(max_length=80, blank=True)
    photo = models.ImageField(upload_to='images/advert/', blank=True, null=True)
    creator = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        get_latest_by = 'created'
        
    def __unicode__(self):
        return unicode(self.title)
        
    def filename(self):
        return os.path.basename(self.photo.name)
    
    
        
  
  
 
    
class AdvertAdmin(admin.ModelAdmin):
    list_display = ['title', 'creator']