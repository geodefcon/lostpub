// в index.html объявлены myMap, myPlacemark и adverts(данные из django).
function init(){
    
    myMap = new ymaps.Map ("map", {
            center: [ymaps.geolocation.latitude, ymaps.geolocation.longitude],
            zoom: 6,
            behaviors: ['default', 'scrollZoom'],
            }),
        myMap.controls
            .add('zoomControl', { top: 75, left: 5 });
    
    for (var i = 0; i < adverts.length; i++) {
        var advert = adverts[i];
        
        if (advert.advtype == 1){
        var typeicon = 'twirl#darkgreenDotIcon';
        var idhead='balloonheaderfind';
        }
        else {
        var typeicon = 'twirl#redDotIcon';
        var idhead = 'balloonheaderlost';
        }
        
        
        var myPlacemark = new ymaps.Placemark([advert.y, advert.x], {
            balloonContentHeader:'<span id='+ idhead+'>'+advert.title+'</span>',
            balloonContentBody:'<a href="#"><img id="balloonthumb_img" onclick="TINY.box.show({image:'+"'"+'/media/images/advert/'+advert.img+"',boxid:'frameless',maskid:'greymask',opacity:20"+'})"\
                src="media/images/advert/thumb/'+advert.img+'"></a>' + '<span id="balloontext">'+advert.body+'</span>'+'<div id="balloonbuff"></div><br><a id='+idhead+' href="#" onclick="TINY.box.show({image:'+"'"+'/media/images/advert/'+advert.img+"',boxid:'frameless',maskid:'greymask',opacity:20"+'})">Фото целиком</a><span id="balloonstamp">' +advert.city + advert.stamp +'</span>'
          
            }, {
            preset: typeicon,
            balloonMaxWidth: 300
            });
        myMap.geoObjects.add(myPlacemark);
        
        
        
    }  
    myMap.events.add('click', function(e){
        myMap.balloon.close();
        });
    
    
};


