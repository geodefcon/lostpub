# -*- coding: utf-8 -*-
from django import forms
from main.models import Advert

class AdvertForm(forms.ModelForm):
    title = forms.CharField(label='Заголовок', max_length=30)
    city = forms.CharField(label='Город',)
    photo = forms.ImageField(required=False, label='Фотография',)
    body = forms.CharField(label='Текст 0 символов из 250', max_length=250,
            widget = forms.Textarea(attrs={'cols':15, 'rows':4}))
    advtype = forms.ChoiceField(label="",choices=Advert.TYPE_CHOICES,
                                widget = forms.RadioSelect())
    
    class Meta:
        model = Advert
        widgets = {'creator': forms.HiddenInput,
                   'longitude':forms.HiddenInput,
                   'latitude':forms.HiddenInput,
                   'country':forms.HiddenInput}
                   
    def clean_photo(self):
        photo = self.cleaned_data['photo']
        try:
            if photo:
                if len(photo) > (1024 * 3072):
                    raise forms.ValidationError(u'Размер файла не должен превышать 3 Mb')
        except AttributeError:
            pass
        return photo
        
    def clean_body(self):
        body = self.cleaned_data['body']
        try:
            if body:
                if len(body) > 250:
                    raise forms.ValidationError(u'Размер текста не должен превышать 250 символов')
        except AttributeError:
            pass
        return body