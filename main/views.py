# -*- coding: utf-8 -*-
from main.models import Advert
from main.forms import AdvertForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response
from board.views import add_csrf
from proj1.settings import MEDIA_ROOT
from board.views import image_resize, paging
from django.utils import simplejson 
from os.path import join as pjoin
import datetime
#from PIL import Image
#import random
import os




def index(request):
    period = request.GET.get('period')  
    atype = request.GET.get('atype')
    if not period:
        period = 7
    delta = datetime.datetime.now() - datetime.timedelta(days=int(period))
    if not atype or int(atype) > 1:
        advs = Advert.objects.filter(created__gte=delta).order_by('-created')
    else:
        advs = Advert.objects.filter(created__gte=delta).filter(advtype=atype).order_by('-created')
    adverts = [ {'adv_id':adv.id, 'x':adv.longitude, 'y':adv.latitude,
                                'advtype':adv.advtype, 'img':adv.filename(),
                                'title':adv.title, 'body':adv.body,
                                'city':adv.city, 'country':adv.country,
                                'stamp':adv.created.strftime('%d-%m-%Y'),
                                } for adv in advs ]
                                
    return render(request, 'main/index.html', {
        'advjson': simplejson.dumps(adverts),
        'user': request.user,
        })

@login_required
def advert(request):
    form = AdvertForm(initial={'creator':request.user,})
    if request.method == 'POST':
        form = AdvertForm(request.POST, request.FILES)
        if form.is_valid:
            try:
                if request.FILES:
                    photoName = request.FILES['photo'].name
                    iname = 'images/advert/' + photoName
                    test = pjoin(MEDIA_ROOT, iname)
                    if os.path.exists(test):
                        razb = photoName.split('.')
                        n = unicode(os.urandom(16).encode('hex'))
                        request.FILES['photo'].name = razb[0] + n + '.' + razb[-1]
                        photoName = request.FILES['photo'].name
                        iname = 'images/advert/' + photoName
                form.save()
                
                if request.FILES:
                    im, temp, format = image_resize(iname, 600)
                    im.save(temp, format)
                
                    im, temp, format = image_resize(iname, 100)
                    iname = 'images/advert/thumb/' + photoName
                    temp = pjoin(MEDIA_ROOT, iname)
                    im.save(temp, format)
                
                return HttpResponseRedirect('/stat/')
            except ValueError:
                pass
    
    
    return render_to_response('main/advert.html', add_csrf(request,
                                                   form=form,
                                                   ))
                                                   
def stat(request):
    period = request.GET.get('period')  
    town = request.GET.get('town')
    params = request.GET.copy()
    path = params.urlencode()
    if not period:
        period = 7
    if not town:
        town = ''
    delta = datetime.datetime.now() - datetime.timedelta(days=int(period))
    adverts = Advert.objects.filter(created__gte=delta).filter(city__icontains=town).order_by('-created')
    records = paging(request, adverts, 40)
    return render_to_response('main/stat.html', {'user':request.user,
                                                   'records':records,
                                                   'period':period,
                                                   'town':town,
                                                   'path':path,
                                                   })
                                                   
                                                   
                                                   


    
