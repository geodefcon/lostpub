# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin
from django.db.models.signals import post_save


class Board(models.Model):
    title = models.CharField(max_length=50)
    def __unicode__(self):
        return unicode(self.title)

class Thread(models.Model):
    title = models.CharField(max_length=50)
    board = models.ForeignKey(Board)
    creator = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ["-changed"]
        get_latest_by = 'changed'
        
    def __unicode__(self):
        return unicode(self.title)
    


class Post(models.Model):
    title = models.CharField(max_length=50, blank=True)
    body = models.TextField(max_length=5000, blank=False)
    thread = models.ForeignKey(Thread)
    creator = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        get_latest_by = 'created'
    
    def __unicode__(self):
        return unicode(self.title)
    
class UserProfile(models.Model):
    city = models.CharField(max_length=30, verbose_name=u'Город', blank=True)
    pet = models.CharField(max_length=30, verbose_name=u'Питомец', blank=True)
    nick = models.CharField(max_length=50, verbose_name=u'Имя', unique=True, null=True)
    avatar = models.ImageField(upload_to='images/', null=True, blank=True)
    user = models.ForeignKey(User)
    
    def __unicode__(self):
        return unicode(self.user)
    
        
    def user_email(self):
        return self.user.email

    
#Admin section

class BoardAdmin(admin.ModelAdmin):
    list_display = ['title']
class ThreadAdmin(admin.ModelAdmin):
    list_display = ['title', 'creator', 'created']
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'thread', 'creator']
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'city', 'user_email']

def create_profile(sender, **kwargs):
    created = kwargs['created']
    if created:
        UserProfile.objects.create(user=kwargs['instance'])
        
post_save.connect(create_profile, sender=User)