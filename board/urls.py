from django.conf.urls.defaults import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('board.views',
    (r"^profile/$", "profile"),
    (r"^(\d{1,2})/add_thread/$", "add_thread"),
    (r"^unit/(\d{1,2})/(\d+)/$", "thread"),
    
    (r"^unit/(\d{1,2})/$", "unit"),
    (r"", "main"),
)
