
ymaps.ready(init);
    var myMap, myPlacemark, coords;

    function init(){     
        myMap = new ymaps.Map ("map", {
            center: [ymaps.geolocation.latitude, ymaps.geolocation.longitude],
            zoom: 10,
            behaviors: ['default', 'scrollZoom'],
            }),
        search = new ymaps.control.SearchControl({noPlacemark:true});
        myMap.controls
            .add(search, { left: '40px', top: '10px' })
            .add('zoomControl', { top: 75, left: 5 });
        coords = myMap.getCenter();
        transferCoords(coords);

        myMap.events.add('click', function (e) {
        coords = e.get('coordPosition');
        transferCoords(coords);
        })
        
        
        myMap.geoObjects.events.add("dragend", function (e) {
        coords = myPlacemark.geometry.getCoordinates();
        transferCoords(coords);
        })
        
        
        search.events.add('resultselect',
        function(){

        var y = search.state.get('currentIndex')
        search.getResult(y).then(function (result){
            coords = result.geometry.getCoordinates();
            transferCoords(coords);
            });
        });
    
        function transferCoords(coords){
            document.getElementById("id_longitude").value=coords[1];
            document.getElementById("id_latitude").value=coords[0];
            document.getElementById("id_city").value =""
            if (myPlacemark) {
                myMap.geoObjects.remove(myPlacemark);
            }
            myPlacemark = new ymaps.Placemark(coords,
            {},{preset: "twirl#redDotIcon", draggable: true});
            myMap.geoObjects.add(myPlacemark);
            
            ymaps.geocode(coords, {kind:'locality'}).then(function(res){
                var tmp = res.geoObjects.get(0).properties.get('metaDataProperty').GeocoderMetaData.AddressDetails.Country;
                    
                document.getElementById("id_country").value=tmp.CountryName;
                if (tmp.Locality !== undefined){
                    document.getElementById("id_city").value=tmp.Locality.LocalityName;
                }
                if (tmp.AdministrativeArea.Locality !== undefined){
                    document.getElementById("id_city").value=tmp.AdministrativeArea.Locality.LocalityName;
                }
                if (tmp.AdministrativeArea.SubAdministrativeArea.Locality !== undefined){
                    document.getElementById("id_city").value=tmp.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                }
            });
        }
        document.getElementById("id_photo").size =11;
        var count = 0
        if (document.getElementById("id_body").value == ''){
            document.getElementById("id_body").style.color='#B5220B';
            document.getElementById("id_body").value='Не забудьте щелчком мыши указать на карте место происшествия';
            
            document.getElementById("id_body").onclick = function(){
            if (count == 0){
                this.value=''
                count++;
                };
            this.style.color='inherit';
            };
        };
        
        document.getElementById("id_body").onkeypress = function(){
            document.getElementsByTagName('label')[3].innerHTML='Текст: '+ (this.value.length+1) +' символов из 250';
            if (this.value.length > 249){
                this.value = this.value.substring(0, 249);
                };
            };
        
         
    }
