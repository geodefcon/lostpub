from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^advert/$', 'main.views.advert'),
    (r'^stat/$', 'main.views.stat'),
    (r'^board/', include('board.urls')),
    (r'^admin/', include(admin.site.urls)),
    (r'accounts/', include('registration.urls')),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT}),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.STATIC_ROOT}),
     (r'^$', 'main.views.index'),                 
)
     

