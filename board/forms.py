# -*- coding: utf-8 -*-
from django import forms
from board.models import UserProfile, Thread, Post
#from django.contrib.auth.models import User

class PostForm(forms.ModelForm):
    body = forms.CharField(label="",
            widget = forms.Textarea(attrs={'cols':70, 'rows':5,}))
    class Meta:
        model = Post
        hi = forms.HiddenInput
        widgets = {'title':hi, 'thread':hi, 'creator':hi}

class ThreadForm(forms.ModelForm):
    class Meta:
        model = Thread
        widgets = {'creator': forms.HiddenInput,}

class ProfileForm(forms.ModelForm):
    city = forms.CharField(required=False, label = 'Город')
    class Meta:
        model = UserProfile
        exclude = ('user',)
    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']
        if avatar:
            try:
                if len(avatar) > (1024 * 3072):
                    raise forms.ValidationError(u'Размер файла не должен превышать 3 Mb')
            except AttributeError:
                pass
            return avatar