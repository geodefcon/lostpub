# -*- coding: utf-8 -*-
from board.models import *
from main.models import Advert
from board.forms import *
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from PIL import Image
import os
from proj1.settings import MEDIA_ROOT, MEDIA_URL
from os.path import join as pjoin

@login_required
def add_thread(request, unitid):
    board = Board.objects.get(pk=unitid)
    form = ThreadForm(initial={'board':board, 'creator':request.user})
    
    if request.method == 'POST':
        form = ThreadForm(request.POST)
        if form.is_valid:
            try:
                form.save()
                latest = unicode(Thread.objects.latest('pk').id)
                return HttpResponseRedirect('/board/unit/'+unitid+'/'+latest+'/')
            except ValueError:
                pass
    return render_to_response('board/add_thread.html', add_csrf(request,
                                                        unitid=unitid,
                                                        form=form,
                                                        ))
                  
                                      
def main(request):
    boards = Board.objects.all()
    return render_to_response('board/main.html', {'boards': boards, 'user':request.user
                                                  })
    
    
def unit(request, unitid):
    threads = Thread.objects.filter(board=unitid)
    records = paging(request,threads, 15)
    return render_to_response('board/unit.html', {'user':request.user,
                                                  'unitid':unitid,
                                                  'records':records})


def thread(request, unitid, threadid):
    posts = Post.objects.all().filter(thread=threadid).order_by('created')
    thread = Thread.objects.get(pk=threadid)
    title = 'Re: ' + thread.title
    page1=''
    if not posts:
        title = thread.title
    records = paging(request, posts, 5)
    
    form = PostForm(initial={'title':title, 'thread':threadid, 'creator':request.user})
    if request.method == 'POST':
        form = PostForm(request.POST)
        page = unicode(records.end_index())
        if form.is_valid:
            try:
                form.save()
                thread.save()
                return HttpResponseRedirect('/board/unit/'+unitid+'/'+threadid+'/?page='+page)
            except ValueError:
                pass
            
            
    return render_to_response('board/thread.html', add_csrf(request,
                                                    records=records,
                                                    unitid=unitid,
                                                    threadid=threadid,
                                                    form=form,
                                                    thread=thread,
                                                    profile=profile))
                                                    
                                                    
@login_required
def profile(request, **kwargs):
    user = UserProfile.objects.get(user=request.user)
    adverts = Advert.objects.filter(creator=request.user).order_by('-created')
    form = ProfileForm(instance=user)
    oldavatar = user.avatar.name
        
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=user)
        if form.is_valid:
            try:
                form.save()
                iname = user.avatar.name
                if iname:
                    im, temp, format = image_resize(iname, 50)
                
                if user.avatar.name != oldavatar:
                    im.save(temp, format)
                    if oldavatar:
                        temp = pjoin(MEDIA_ROOT, oldavatar)
                        os.remove(temp)
                return HttpResponseRedirect('board')
            except ValueError:
                pass
                
    return render_to_response('board/profile.html', add_csrf(request,
                                                    form=form,
                                                    adverts=adverts,
                                                    ))
                                                     
                                                     
def paging(request, data, records_onpage):
    paginator = Paginator(data, records_onpage)
    page = request.GET.get('page')
    try:
        records = paginator.page(page)
    except PageNotAnInteger:
        records = paginator.page(1)
    except EmptyPage:
        records = paginator.page(paginator.num_pages)
    return records


def image_resize(iname, size):
    temp = pjoin(MEDIA_ROOT, iname)
    im = Image.open(temp)
    format = im.format
    if im.mode not in ('L', 'RGBA'):
        im = im.convert('RGBA')
    im.thumbnail((size,size), Image.ANTIALIAS)
    return im, temp, format
    
def add_csrf(request, **kwargs):
    d = dict(user=request.user, **kwargs)
    d.update(csrf(request))
    return d